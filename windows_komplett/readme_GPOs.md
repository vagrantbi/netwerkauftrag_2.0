READme

Set Wallpaper for Clients:

Go to Server Manager -> Tools -> Group Policy Management 
Open Forest: fc-master.ch -> Domains -> fc-master.ch
Rightklick Wallpaper -> Edit
Under User Configuration -> Open Policies -> Administratives Templates -> Desktop -> Desktop
Doubleklick "Enable Active Desktop" -> Select "Enable" -> OK
Doubleklick "Desktop Wallpaper" -> Select "Enable" -> Under Wallpaper Name paste "C:\tmp\fc-master.png" -> OK


Set "www.bedag.ch" as Homepage for MSEdge:

Go to Server Manager -> Tools -> Group Policy Management 
Open Forest: fc-master.ch -> Domains -> fc-master.ch
Rightklick MSEdge -> Edit
Under User Configuration -> Open Policies -> Administratives Templates -> Microsoft Edge -> Startup, home page and new tap page
Doubleklick "Action to take on startup" -> Select "Enable" -> Under Options, select "Open a list of URLs" -> OK
Doubleklick "Sites to open when the browser starts" -> Select "Enable" -> Under Options klick "Show...", insert "https://www.gmx.ch" -> OK -> OK


If the Client ist already running run the command "gpupdate /force" and the policy settings should now work. Congrats.

