# Permission Full Control share "Produktion" 

#Disable Inheritance
$acl = Get-Acl -Path "C:\\shares\Produktion"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\\shares\Produktion"

#Full Control for User

$acl7 = Get-Acl -Path "C:\\shares\Produktion"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\\shares\Produktion"