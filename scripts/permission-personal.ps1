# Permission Full Control share "Personal" 

#Disable Inheritance
$acl = Get-Acl -Path "C:\\shares\Personal"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\\shares\Personal"

#Full Control for User

$acl1 = Get-Acl -Path "C:\\shares\Personal"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\rqan485216","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\\shares\Personal"

$acl2 = Get-Acl -Path "C:\\shares\Personal"
$AccessRule2 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\huar485217","FullControl","Allow")
$acl2.SetAccessRule($AccessRule2)
$acl2 | Set-Acl -Path "C:\\shares\Personal"

$acl7 = Get-Acl -Path "C:\\shares\Personal"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\\shares\Personal"