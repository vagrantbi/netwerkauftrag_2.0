# Title: Set-Settings
# Comments: This script is used to set the correct keyboard layout.
# Created by Dorothea Vlad
# Date: 29.01.2021
# Version: 0.1

#Script to set the Windows settings
# Show LanguageList
$OldList = Get-WinUserLanguageList
# Add de-CH to LanguageList
$OldList.Add("de-CH")
Set-WinUserLanguageList -LanguageList $OldList -Force
Set-WinUserLanguageList -LanguageList de-CH -Force