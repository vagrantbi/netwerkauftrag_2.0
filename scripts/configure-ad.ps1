# Title: Configure AD, Insert new users in an AD, Insert new users in an AD
# Comments: This script is used to configure the AD. Also it is used to add new Ous and new AD-User
# Created by Dorothea Vlad
# Date: 11.02.2021
# Version: 0.1

# wait until we can access the AD. this is needed to prevent errors like:
#   Unable to find a default server with Active Directory Web Services running.
while ($true) {
    try {
        Get-ADDomain | Out-Null
        break
    }
    catch {
        Start-Sleep -Seconds 10
    }
}


$adDomain = Get-ADDomain
$domain = $adDomain.DNSRoot
$domainDn = $adDomain.DistinguishedName
$usersAdPath = "CN=Users,$domainDn"
$password = ConvertTo-SecureString -AsPlainText 'Welcome!20' -Force


# remove the non-routable vagrant nat ip address from dns.
# NB this is needed to prevent the non-routable ip address from
#    being registered in the dns server.
# NB the nat interface is the first dhcp interface of the machine.
$vagrantNatAdapter = Get-NetAdapter -Physical `
| Where-Object { $_ | Get-NetIPAddress | Where-Object { $_.PrefixOrigin -eq 'Dhcp' } } `
| Sort-Object -Property Name `
| Select-Object -First 1
$vagrantNatIpAddress = ($vagrantNatAdapter | Get-NetIPAddress).IPv4Address
# remove the $domain nat ip address resource records from dns.
$vagrantNatAdapter | Set-DnsClient -RegisterThisConnectionsAddress $false
Get-DnsServerResourceRecord -ZoneName $domain -Type 1 `
| Where-Object { $_.RecordData.IPv4Address -eq $vagrantNatIpAddress } `
| Remove-DnsServerResourceRecord -ZoneName $domain -Force
# disable ipv6.
$vagrantNatAdapter | Disable-NetAdapterBinding -ComponentID ms_tcpip6
# remove the dc.$domain nat ip address resource record from dns.
$dnsServerSettings = Get-DnsServerSetting -All
$dnsServerSettings.ListeningIPAddress = @(
    $dnsServerSettings.ListeningIPAddress `
    | Where-Object { $_ -ne $vagrantNatIpAddress }
)
Set-DnsServerSetting $dnsServerSettings
# flush the dns client cache.
Clear-DnsClientCache

# set the Administrator password.
# NB this is also an Domain Administrator account.
Set-ADAccountPassword `
    -Identity "CN=Administrator,$usersAdPath" `
    -Reset `
    -NewPassword $password
Set-ADUser `
    -Identity "CN=Administrator,$usersAdPath" `
    -PasswordNeverExpires $true


#Installing the Active Directory Domain Services Role
Install-windowsfeature -name AD-Domain-Services -IncludeManagementTools

#Add new OUs with a new directory and a testfile.
New-ADOrganizationalUnit -Name "IT" -Path "DC=fc-master,DC=ch"
New-Item -Path "C:\\shares\IT" -Name "IT" -ItemType "directory"
New-Item -Path "C:\\shares\IT" -Name "textfile1.txt" -ItemType "file" -Value "This is a text string."
New-ADOrganizationalUnit -Name "CEO" -Path "DC=fc-master,DC=ch"
New-Item -Path "C:\\shares\CEO" -Name "CEO" -ItemType "directory"
New-Item -Path "C:\\shares\CEO" -Name "textfile2.txt" -ItemType "file" -Value "This is a text string."
New-ADOrganizationalUnit -Name "Personal" -Path "DC=fc-master,DC=ch"
New-Item -Path "C:\\shares\Personal" -Name "Personal" -ItemType "directory"
New-Item -Path "C:\\shares\Personal" -Name "textfile3.txt" -ItemType "file" -Value "This is a text string."
New-ADOrganizationalUnit -Name "Entwicklung" -Path "DC=fc-master,DC=ch"
New-Item -Path "C:\\shares\Entwicklung" -Name "intern-FLY" -ItemType "directory"
New-Item -Path "C:\\shares\Entwicklung" -Name "sensibel" -ItemType "directory"
New-Item -Path "C:\\shares\Entwicklung\sensibel" -Name "alle" -ItemType "directory"
New-Item -Path "C:\\shares\Entwicklung\sensibel" -Name "htbr44455618" -ItemType "directory"
New-Item -Path "C:\\shares\Entwicklung\sensibel" -Name "kbbr44455619" -ItemType "directory"
New-Item -Path "C:\\shares\Entwicklung\sensibel" -Name "jcxc44455620" -ItemType "directory"
New-Item -Path "C:\\shares\Entwicklung\sensibel" -Name "uqxr44455621" -ItemType "directory"
New-ADOrganizationalUnit -Name "BSD" -Path "DC=fc-master,DC=ch"
New-Item -Path "C:\\shares\BSD" -Name "intern" -ItemType "directory"
New-Item -Path "C:\\shares\BSD" -Name "sensibel" -ItemType "directory"
New-ADOrganizationalUnit -Name "Marketing" -Path "DC=fc-master,DC=ch"
New-Item -Path "C:\\shares\Marketing" -Name "Marketing" -ItemType "directory"
New-Item -Path "C:\\shares\Marketing" -Name "textfile4.txt" -ItemType "file" -Value "This is a text string."
New-ADOrganizationalUnit -Name "Produktion" -Path "DC=fc-master,DC=ch"
New-Item -Path "C:\\shares\Produktion" -Name "Produktion" -ItemType "directory"
New-Item -Path "C:\\shares\Produktion" -Name "textfile5.txt" -ItemType "file" -Value "This is a text string."

# Insert new AD-Users 
# Sort them all into specific OU
New-ADUser -Name "user1" -GivenName user1 -SamAccountName user1 -UserPrincipalName user1@fc-master.ch  -AccountPassword $password -Path 'OU=IT, DC=fc-master, DC=ch' -Enabled $true -PasswordNeverExpires $true
New-AdUser -Path $usersAdPath -Name "vmadmin" -UserPrincipalName vmadmin@$domain -GivenName vmadmin -DisplayName vmadmin -AccountPassword $password -Enabled $true -PasswordNeverExpires $true
New-ADUser -Name "Stefan Bandara" -GivenName Stefan -Surname Bandara -SamAccountName uhou495413 -UserPrincipalName uhou495413@fc-master.ch -Path 'OU=IT, DC=fc-master, DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Marc Suter" -GivenName Marc -Surname Suter -SamAccountName plqk495414 -UserPrincipalName plqk495414@fc-master.ch -Path 'OU=IT,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Laura Krebs" -GivenName Laura -Surname Krebs -SamAccountName puxp495415 -UserPrincipalName puxp495415@fc-master.ch -Path 'OU=IT,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Daniel Lipo" -GivenName Daniel -Surname Lipo -SamAccountName rbbn43454F12 -UserPrincipalName rbbn43454F12@fc-master.ch -Path 'OU=CEO,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Hannes Gimbal" -GivenName Hannes -Surname Gimbal -SamAccountName rqan485216 -UserPrincipalName rqan485216@fc-master.ch -Path 'OU=Personal,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Susanna Gerber" -GivenName Susanna -Surname Gerber -SamAccountName huar485217 -UserPrincipalName uhuar485217@fc-master.ch -Path 'OU=Personal,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Bernd Leuenberger" -GivenName Bernd -Surname Leuenberger -SamAccountName htbr44455618 -UserPrincipalName htbr44455618@fc-master.ch -Path 'OU=Entwicklung,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Paul Lehmann" -GivenName Paul -Surname Lehmann -SamAccountName kbbr44455619 -UserPrincipalName kbbr44455619@fc-master.ch -Path 'OU=Entwicklung,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Reto Kohler" -GivenName Reto -Surname Kohler -SamAccountName jcxc44455620 -UserPrincipalName jcxc44455620@fc-master.ch -Path 'OU=Entwicklung,DC=fc-master,DC=ch'-PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Thomas Keller" -GivenName Thomas -Surname Keller -SamAccountName uqxr44455621 -UserPrincipalName uqxr44455621@fc-master.ch -Path 'OU=Entwicklung,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Sarah Gehri" -GivenName Sarah -Surname Gehri -SamAccountName uyar4D5622 -UserPrincipalName uyar4D5622@fc-master.ch -Path 'OU=Marketing,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Fritz Gugger" -GivenName Fritz -Surname Gugger -SamAccountName jiuk42534423 -UserPrincipalName jiuk42534423@fc-master.ch -Path 'OU=BSD,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Markus König" -GivenName Markus -Surname König -SamAccountName kqxcr42534424 -UserPrincipalName kqxcr42534424@fc-master.ch -Path 'OU=BSD,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Jakob Zenger" -GivenName Jakob -Surname Zenger -SamAccountName coir42534425 -UserPrincipalName coir42534425@fc-master.ch -Path 'OU=BSD,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Bruno Scherz" -GivenName Bruno -Surname Scherz -SamAccountName hcql42534426 -UserPrincipalName hcql42534426@fc-master.ch -Path 'OU=BSD,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true
New-ADUser -Name "Johanna Wyss" -GivenName Johanna -Surname Wyss -SamAccountName hume42534427 -UserPrincipalName hume42534427@fc-master.ch -Path 'OU=BSD,DC=fc-master,DC=ch' -PasswordNeverExpires $true -AccountPassword $password -Enabled $true