# Permission Full Control share "Marketing" 

#Disable Inheritance
$acl = Get-Acl -Path "C:\\shares\Marketing"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\\shares\Marketing"

#Full Control for User

$acl1 = Get-Acl -Path "C:\\shares\Marketing"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\uyar4D5622","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\\shares\Marketing"

$acl7 = Get-Acl -Path "C:\\shares\Marketing"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\\shares\Marketing"