# Title: Enable UAC
# Comments: This script is used to enable UAC.
# Created by Dorothea Vlad
# Date: 03.02.2021
# Version: 0.1

#Set UAC (User account control) to enabled. Computer reboot.
Set-ItemProperty -Path HKLM:Software\Microsoft\Windows\CurrentVersion\policies\system -Name EnableLUA -Value 1 -Force