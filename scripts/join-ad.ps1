# Title: Script to join the fc-master.ch domain
# Comments: This script is used to join the domain
# Created by Dorothea Vlad
# Date: 11.02.2021
# Version: 0.1 


$domain = "fc-master.ch"
$user = "vagrant\Administrator"
$password = ConvertTo-SecureString -AsPlainText -Force -String "Welcome!20"
$adlogin = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $user, $password
Write-Host "Joining domain $domain, please wait..."
try {
    Add-Computer -DomainName $domain -Credential $adlogin -ErrorAction Stop
}
catch {
    Write-Host "Could not join domain $domain!! Please check for errors."
}
