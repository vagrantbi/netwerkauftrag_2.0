# Title: Set-DNS
# Comments: This script is used to set a DNS.
# Created by Bruce Reber
# Date: 12.02.2021
# Version: 0.2


#This script sets the DNS on the server and the client to the ip of the DC

# Get the Interface ID of the Ethernet Interface
$ifIndex = (Get-NetAdapter | Where-Object { $_.Name -like "Ethernet" }).ifIndex
$ifIndex2 = (Get-NetAdapter | Where-Object { $_.Name -like "Ethernet 2" }).ifIndex
$dnsserver = ("192.168.20.41") 

# Set the DNS server
Set-DnsClientServerAddress -InterfaceIndex $ifIndex -ServerAddresses $dnsserver
Set-DnsClientServerAddress -InterfaceIndex $ifIndex2 -ServerAddresses $dnsserver
