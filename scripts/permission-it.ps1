# Permission Full Control share "IT" 

#Disable Inheritance
$acl = Get-Acl -Path "C:\\shares\IT"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\\shares\IT"

#Full Control for User

$acl1 = Get-Acl -Path "C:\\shares\IT"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\uhou495413","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\\shares\IT"

$acl2 = Get-Acl -Path "C:\\shares\IT"
$AccessRule2 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\plqk495414","FullControl","Allow")
$acl2.SetAccessRule($AccessRule2)
$acl2 | Set-Acl -Path "C:\\shares\IT"

$acl3 = Get-Acl -Path "C:\\shares\IT"
$AccessRule3 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\puxp495415","FullControl","Allow")
$acl3.SetAccessRule($AccessRule3)
$acl3 | Set-Acl -Path "C:\\shares\IT"

$acl7 = Get-Acl -Path "C:\\shares\IT"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\\shares\IT"