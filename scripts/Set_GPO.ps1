# Title: Set-GPO
# Comments: This script is used to set the blank GPOs on the Srv01
# Created by Bruce Reber
# Date: 30.04.2021
# Version: 0.1


Import-Module grouppolicy
New-GPO -Name Wallpaper | New-GPLink -Target "DC=fc-master,DC=ch" -LinkEnabled Yes

Import-Module grouppolicy
New-GPO -Name MSEdge | New-GPLink -Target "DC=fc-master,DC=ch" -LinkEnabled Yes