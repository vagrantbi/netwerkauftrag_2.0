# Title: DHCP
# Comments: This script is used to set a DHCP Scope.
# Created by Bruce Reber
# Date: 12.02.2021
# Version: 0.1


# dhcp scope variables
$scopename = "MyAutoScope"
$startrange = "192.168.20.40"
$endrange = "192.168.20.90"
$subnetmask = "255.255.255.0"
$scopeID = "192.168.20.0"

# Creating scope
Add-DHCPServerv4Scope -EndRange $endrange -Name $scopename -StartRange $startrange -SubnetMask $subnetmask -State Active

# Set-DHCPServerv4OptionValue -ScopeId $scopeID
Set-DhcpServerv4OptionValue -ScopeId $scopeID -DnsServer 192.168.20.10