# Permission Full Control share "Entwicklung" 

#Disable Inheritance part 1
$acl = Get-Acl -Path "C:\\shares\Entwicklung"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\\shares\Entwicklung"

#Full Control for User

$acl1 = Get-Acl -Path "C:\\shares\Entwicklung"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\htbr44455618","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\\shares\Entwicklung"

$acl2 = Get-Acl -Path "C:\\shares\Entwicklung"
$AccessRule2 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\jcxc44455620","FullControl","Allow")
$acl2.SetAccessRule($AccessRule2)
$acl2 | Set-Acl -Path "C:\\shares\Entwicklung"

$acl3 = Get-Acl -Path "C:\\shares\Entwicklung"
$AccessRule3 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\kbbr44455619","FullControl","Allow")
$acl3.SetAccessRule($AccessRule3)
$acl3 | Set-Acl -Path "C:\\shares\Entwicklung"

$acl4 = Get-Acl -Path "C:\\shares\Entwicklung"
$AccessRule4 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\uqxr44455621","FullControl","Allow")
$acl4.SetAccessRule($AccessRule4)
$acl4 | Set-Acl -Path "C:\\shares\Entwicklung"

$acl7 = Get-Acl -Path "C:\shares\Entwicklung"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\shares\Entwicklung"

#Disable Inheritance part 2
$acl = Get-Acl -Path "C:\shares\Entwicklung\intern-FLY"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\shares\Entwicklung\intern-FLY"

#Full Control for User

$acl1 = Get-Acl -Path "C:\shares\Entwicklung\intern-FLY"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\htbr44455618","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\shares\Entwicklung\intern-FLY"

$acl2 = Get-Acl -Path "C:\shares\Entwicklung\intern-FLY"
$AccessRule2 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\jcxc44455620","FullControl","Allow")
$acl2.SetAccessRule($AccessRule2)
$acl2 | Set-Acl -Path "C:\shares\Entwicklung\intern-FLY"

$acl3 = Get-Acl -Path "C:\shares\Entwicklung\intern-FLY"
$AccessRule3 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\kbbr44455619","FullControl","Allow")
$acl3.SetAccessRule($AccessRule3)
$acl3 | Set-Acl -Path "C:\shares\Entwicklung\intern-FLY"

$acl4 = Get-Acl -Path "C:\shares\Entwicklung\intern-FLY"
$AccessRule4 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\uqxr44455621","FullControl","Allow")
$acl4.SetAccessRule($AccessRule4)
$acl4 | Set-Acl -Path "C:\shares\Entwicklung\intern-FLY"

$acl5 = Get-Acl -Path "C:\shares\Entwicklung\intern-FLY"
$AccessRule5 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\uqxr44455621","READ","Allow")
$acl5.SetAccessRule($AccessRule5)
$acl5 | Set-Acl -Path "C:\shares\Entwicklung\intern-FLY"

$acl7 = Get-Acl -Path "C:\shares\Entwicklung\intern-FLY"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\shares\Entwicklung\intern-FLY"

#Disable Inheritance part 3
$acl = Get-Acl -Path "C:\shares\Entwicklung\sensibel"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\shares\Entwicklung\sensibel"

#Full Control for User

$acl1 = Get-Acl -Path "C:\shares\Entwicklung\sensibel"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\htbr44455618","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\shares\Entwicklung\sensibel"

$acl2 = Get-Acl -Path "C:\shares\Entwicklung\sensibel"
$AccessRule2 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\jcxc44455620","FullControl","Allow")
$acl2.SetAccessRule($AccessRule2)
$acl2 | Set-Acl -Path "C:\shares\Entwicklung\sensibel"

$acl3 = Get-Acl -Path "C:\shares\Entwicklung\sensibel"
$AccessRule3 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\kbbr44455619","FullControl","Allow")
$acl3.SetAccessRule($AccessRule3)
$acl3 | Set-Acl -Path "C:\shares\Entwicklung\sensibel"

$acl4 = Get-Acl -Path "C:\shares\Entwicklung\sensibel"
$AccessRule4 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\uqxr44455621","FullControl","Allow")
$acl4.SetAccessRule($AccessRule4)
$acl4 | Set-Acl -Path "C:\shares\Entwicklung\sensibel"

$acl7 = Get-Acl -Path "C:\shares\Entwicklung\sensibel"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\shares\Entwicklung\sensibel"

#Disable Inheritance part 4
$acl = Get-Acl -Path "C:\shares\Entwicklung\sensibel\htbr44455618"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\shares\Entwicklung\sensibel\htbr44455618"

#Full Control for User

$acl1 = Get-Acl -Path "C:\shares\Entwicklung\sensibel\htbr44455618"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\htbr44455618","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\shares\Entwicklung\sensibel\htbr44455618"

$acl7 = Get-Acl -Path "C:\shares\Entwicklung\sensibel\htbr44455618"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\shares\Entwicklung\sensibel\htbr44455618"

#Disable Inheritance part 5
$acl = Get-Acl -Path "C:\shares\Entwicklung\sensibel\jcxc44455620"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\shares\Entwicklung\sensibel\jcxc44455620"

#Full Control for User

$acl1 = Get-Acl -Path "C:\shares\Entwicklung\sensibel\jcxc44455620"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\jcxc44455620","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\shares\Entwicklung\sensibel\jcxc44455620"

$acl7 = Get-Acl -Path "C:\shares\Entwicklung\sensibel\jcxc44455620"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\shares\Entwicklung\sensibel\jcxc44455620"

#Disable Inheritance part 6
$acl = Get-Acl -Path "C:\shares\Entwicklung\sensibel\kbbr44455619"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\shares\Entwicklung\sensibel\kbbr44455619"

#Full Control for User

$acl1 = Get-Acl -Path "C:\shares\Entwicklung\sensibel\kbbr44455619"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\kbbr44455619","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\shares\Entwicklung\sensibel\kbbr44455619"

acl7 = Get-Acl -Path "C:\shares\Entwicklung\sensibel\kbbr44455619"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\shares\Entwicklung\sensibel\kbbr44455619"

#Disable Inheritance part 7
$acl = Get-Acl -Path "C:\shares\Entwicklung\sensibel\uqxr44455621"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\shares\Entwicklung\sensibel\uqxr44455621"

#Full Control for User

$acl1 = Get-Acl -Path "C:\shares\Entwicklung\sensibel\uqxr44455621"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\uqxr44455621","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\shares\Entwicklung\sensibel\uqxr44455621"

acl7 = Get-Acl -Path "C:\shares\Entwicklung\sensibel\uqxr44455621"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\shares\Entwicklung\sensibel\uqxr44455621"

