# Permission Full Control share "BSD" 

#Disable Inheritance part 1
$acl = Get-Acl -Path "C:\\shares\BSD"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\\shares\BSD"

#Full Control for User

$acl1 = Get-Acl -Path "C:\\shares\BSD"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\jiuk42534423","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\\shares\BSD"

$acl2 = Get-Acl -Path "C:\\shares\BSD"
$AccessRule2 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\kqxcr42534424","FullControl","Allow")
$acl2.SetAccessRule($AccessRule2)
$acl2 | Set-Acl -Path "C:\\shares\BSD"

$acl3 = Get-Acl -Path "C:\\shares\BSD"
$AccessRule3 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\coir42534425","FullControl","Allow")
$acl3.SetAccessRule($AccessRule3)
$acl3 | Set-Acl -Path "C:\\shares\BSD"

$acl4 = Get-Acl -Path "C:\\shares\BSD"
$AccessRule4 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\hcql42534426","FullControl","Allow")
$acl4.SetAccessRule($AccessRule4)
$acl4 | Set-Acl -Path "C:\\shares\BSD"

$acl5 = Get-Acl -Path "C:\\shares\BSD"
$AccessRule5 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\hume42534427","FullControl","Allow")
$acl5.SetAccessRule($AccessRule5)
$acl5 | Set-Acl -Path "C:\\shares\BSD"

$acl6 = Get-Acl -Path "C:\shares\BSD"
$AccessRule6 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\rbbn43454F12","READ","Allow")
$acl6.SetAccessRule($AccessRule6)
$acl6 | Set-Acl -Path "C:\shares\BSD"

$acl7 = Get-Acl -Path "C:\shares\BSD"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\shares\BSD"

#Disable Inheritance part 2
$acl = Get-Acl -Path "C:\shares\BSD\intern"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\shares\BSD\intern"

#Full Control for User

$acl1 = Get-Acl -Path "C:\shares\BSD\intern"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\jiuk42534423","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\shares\BSD\intern"

$acl2 = Get-Acl -Path "C:\shares\BSD\intern"
$AccessRule2 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\kqxcr42534424","FullControl","Allow")
$acl2.SetAccessRule($AccessRule2)
$acl2 | Set-Acl -Path "C:\shares\BSD\intern"

$acl3 = Get-Acl -Path "C:\shares\BSD\intern"
$AccessRule3 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\coir42534425","FullControl","Allow")
$acl3.SetAccessRule($AccessRule3)
$acl3 | Set-Acl -Path "C:\shares\BSD\intern"

$acl4 = Get-Acl -Path "C:\shares\BSD\intern"
$AccessRule4 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\hcql42534426","FullControl","Allow")
$acl4.SetAccessRule($AccessRule4)
$acl4 | Set-Acl -Path "C:\shares\BSD\intern"

$acl5 = Get-Acl -Path "C:\shares\BSD\intern"
$AccessRule5 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\hume42534427","FullControl","Allow")
$acl5.SetAccessRule($AccessRule5)
$acl5 | Set-Acl -Path "C:\shares\BSD\intern"

$acl6 = Get-Acl -Path "C:\shares\BSD\intern"
$AccessRule6 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl6.SetAccessRule($AccessRule6)
$acl6 | Set-Acl -Path "C:\shares\BSD\intern"

#Disable Inheritance part 3
$acl = Get-Acl -Path "C:\\shares\BSD\sensibel"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\\shares\BSD\sensibel"

#Full Control for User

$acl1 = Get-Acl -Path "C:\shares\BSD\sensibel"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\rbbn43454F12","READ","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\shares\BSD\sensibel"

$acl6 = Get-Acl -Path "C:\shares\BSD\sensibel"
$AccessRule6 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl6.SetAccessRule($AccessRule6)
$acl6 | Set-Acl -Path "C:\shares\BSD\sensibel"