# Permission Full Control share "CEO" 

#Disable Inheritance
$acl = Get-Acl -Path "C:\\shares\CEO"
$acl.SetAccessRuleProtection($true,$false)
$acl | Set-Acl -Path "C:\\shares\CEO"

#Full Control for User

$acl1 = Get-Acl -Path "C:\\shares\CEO"
$AccessRule1 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\rbbn43454F12","FullControl","Allow")
$acl1.SetAccessRule($AccessRule1)
$acl1 | Set-Acl -Path "C:\\shares\CEO"

$acl7 = Get-Acl -Path "C:\\shares\CEO"
$AccessRule7 = New-Object System.Security.AccessControl.FileSystemAccessRule("fc-master\vagrant","FullControl","Allow")
$acl7.SetAccessRule($AccessRule7)
$acl7 | Set-Acl -Path "C:\\shares\CEO"